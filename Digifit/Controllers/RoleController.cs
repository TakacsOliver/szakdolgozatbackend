﻿using Digifit.Models.Query;
using Digifit.Utils;
using Microsoft.AspNetCore.Mvc;
using Digifit.Models.Database;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Digifit.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RoleController : ControllerBase
    {
        private readonly DigifitContext _context;
        private readonly JwtTokenHandler _jwtTokenHandler;

        public RoleController(DigifitContext context, JwtTokenHandler jwtTokenHandler)
        {
            _context = context;
            _jwtTokenHandler = jwtTokenHandler;
        }

        [HttpPost("user")]
        public async Task<IActionResult> GetUserRolesAsync([FromBody] Models.Authentication.TokenRequest tokenRequest)
        {
          
            string token = tokenRequest.Token;
 

            if (string.IsNullOrEmpty(token))
            {
                return BadRequest("Invalid token");
            }

            if (!_jwtTokenHandler.IsTokenValid(token))
            {
                return StatusCode(498, "Token expired or invalid"); // 498: Token expired/invalid (custom code)
            }

            Guid userId = _jwtTokenHandler.GetUserIdFromToken(token);

            // Kinyerjük a felhasználó szerepköreit
            var userRoles = await GetUserRolesAsync(userId);

            Console.WriteLine(token);
            Console.WriteLine(userId);
            Console.WriteLine(userRoles);

            return Ok(userRoles);
        }

        private async Task<string[]> GetUserRolesAsync(Guid userId)
        {
            var userRoles = await _context.UserRole
                .Where(ur => ur.UserId == userId)
                .Select(ur => ur.Role.RoleName)
                .ToArrayAsync();

            return userRoles;
        }
    }
}
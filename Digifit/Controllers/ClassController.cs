﻿using Digifit.DAL;
using Digifit.Models.Database;
using Digifit.Utils;
using Microsoft.AspNetCore.Mvc;
using Digifit.Models.Query;
using Microsoft.AspNetCore.Authorization;
using Microsoft.EntityFrameworkCore;
using static Microsoft.EntityFrameworkCore.DbLoggerCategory;
using System.Text.Json;
using Azure.Core;
using Newtonsoft.Json.Linq;


namespace Digifit.Controllers
{
    //[Authorize(Roles = "Teacher")]
    [Route("api/[controller]")]
    [ApiController]
    public class ClassController : Controller
    {

        private readonly DigifitContext _context;
        private readonly JwtTokenHandler _jwtTokenHandler;

        public ClassController(DigifitContext context, JwtTokenHandler jwtTokenHandler)
        {
            _context = context;
            _jwtTokenHandler = jwtTokenHandler;
        }
        /*
        [HttpPost("get-courses")]
        public IActionResult CoursesData([FromBody] CoursesRequest request)
        {
            string token = request.Token;


            if (string.IsNullOrEmpty(token))
            {
                return BadRequest("Invalid token");
            }

            if (!_jwtTokenHandler.IsTokenValid(token))
            {
                return StatusCode(498, "Token expired or invalid");
            }

            Guid userId = _jwtTokenHandler.GetUserIdFromToken(token);

            System.Console.WriteLine("userId: " + userId);

            System.Console.WriteLine("HALKOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO ");
            
            var courses = _context.TeacherWorkout
                .Where(tw => tw.UserId == userId)
                .Include(tw => tw.Workout)
                .Select(tw => new {
                    Id = tw.WorkoutId,
                    Name = tw.Workout.Name
                })
                .ToList();
            
            System.Console.WriteLine("courseNames: " + courses);

            foreach (var courseName in courses)
            {
                System.Console.WriteLine(courseName.Id);
                System.Console.WriteLine(courseName.Name);
            }


            return Ok(courses);
        }
        */
        [HttpPut("save-grade")]
        public IActionResult SaveGrade([FromBody] GradeUpdate request)
        {
            Guid lessonId = request.LessonId;
            Guid userId = request.UserId;
            int grade = request.Grade;

            var studentGrade = _context.StudentGrade
                .FirstOrDefault(sg => sg.UserId == userId && sg.LessonId == lessonId);

            if (studentGrade != null )
            {
                // Ha létezik, frissítsük a meglévő rekordot
                studentGrade.Grade = grade;
                _context.StudentGrade.Update(studentGrade);
            }
            else
            {
                // Ha nem létezik, hozzunk létre egy új rekordot
                studentGrade = new StudentGrade
                {
                    StudentGradeId = Guid.NewGuid(), // Győződj meg róla, hogy egyedi azonosítót generálsz
                    UserId = userId,
                    LessonId = lessonId,
                    Grade = grade
                };
                _context.StudentGrade.Add(studentGrade);
            }

            _context.SaveChanges();

            return Ok(new { message = "Grade updated successfully" });
        }


        [HttpPost("get-students")]
        public IActionResult StudentsData([FromBody] StudentsRequest request)
        {
            Guid lessonId = request.LessonId;
            Console.WriteLine(lessonId);
            Console.WriteLine(request.LessonId);

            var students = _context.StudentLesson
                .Where(sl => sl.LessonId == lessonId)
                .Include(sl => sl.User)
                .GroupBy(sl => sl.User.Id)
                .Select(group => new
                {
                    UserId = group.Key,
                    group.First().User.Neptun,
                    WorkoutCount = group.Count(),
                    Grade = _context.StudentGrade
                        .Where(sg => sg.LessonId == lessonId && sg.UserId == group.Key)
                        .Select(sg => sg.Grade)
                        .FirstOrDefault()
                })
                .ToList();

            string studentsJson = JsonSerializer.Serialize(students, new JsonSerializerOptions { WriteIndented = true });
            Console.WriteLine(studentsJson);

            return Ok(students);
        }
    

        [HttpPost("get-lessons")]
        public IActionResult LessonsData([FromBody] CoursesRequest request)
        { 
              string token = request.Token;


            if (string.IsNullOrEmpty(token))
            {
                return BadRequest("Invalid token");
            }

            if (!_jwtTokenHandler.IsTokenValid(token))
            {
                return StatusCode(498, "Token expired or invalid");
            }


            Guid userId = _jwtTokenHandler.GetUserIdFromToken(token);

            var teacherLessons = _context.TeacherLesson
                                        .Where(tl => tl.UserId == userId)
                                        .Select(tl => new { Id = tl.Lesson.LessonId, Name = tl.Lesson.LessonName }) 
                                        .ToList();


            return Ok(teacherLessons);

        }

        [HttpPost("get-workouts")]
        public IActionResult WorkoutsData([FromBody] LessonsRequest request)
        {
            Guid userId = request.studentId;
            Guid lessonId = request.lessonId;

            // Edzések lekérése a felhasználóhoz és a leckéhez
            var workouts = _context.UserWorkoutExercise
                .Where(uwe => uwe.User.Id == userId && uwe.Workout.StudentLessons.Any(sl => sl.LessonId == lessonId))
                .Select(uwe => new
                {
                    uwe.Workout.WorkoutId,
                    uwe.Workout.Name,
                    uwe.Workout.StartDate,
                    uwe.Workout.EndDate,
                    uwe.Performance,
                    ExerciseCount = uwe.Workout.UserWorkoutExercises.Count  // Gyakorlatok számának lekérése
                })
                .ToList();

            // Átlagos teljesítmény kiszámítása minden edzéshez
            var workoutsWithAveragePerformance = workouts
                .GroupBy(w => w.WorkoutId)
                .Select(group => new
                {
                    WorkoutId = group.Key,
                    group.First().Name,
                    group.First().StartDate,
                    group.First().EndDate,
                    Performance = group.Average(w => w.Performance),
                    group.First().ExerciseCount
                })
                .ToList();

            return Ok(workoutsWithAveragePerformance);
        }

        [HttpPost("get-details")]
        public IActionResult DetailsData([FromBody] DetailsRequest request)
        {
            Guid userId = request.UserId;
            Guid workoutId = request.WorkoutId;

            var userWorkoutExercises = _context.UserWorkoutExercise
                .Where(uwe => uwe.User.Id == userId && uwe.Workout.WorkoutId == workoutId)
                .Select(uwe => new
                {
                    uwe.Exercise.ExerciseId,
                    ExerciseName = uwe.Exercise.Name,
                    ExerciseDuration = uwe.Exercise.Duration,
                    uwe.Performance
                })
                .ToList();


            return Ok(userWorkoutExercises);
        }

    }

}
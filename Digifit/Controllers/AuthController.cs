﻿using Google.Apis.Auth;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Newtonsoft.Json;
using Microsoft.EntityFrameworkCore;
using Digifit.DAL;
using AutoMapper;
using Digifit.Models.Authentication;
using System.Security.Cryptography;
using System.Linq;
using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;
using Azure.Core;
using FluentMigrator.Infrastructure;
using static Google.Apis.Requests.BatchRequest;
using System;
using Digifit.Models.Database;

namespace Digifit.Controllers
{
    [Controller]
    public class AuthController : Controller
    {
        private readonly IUserRepository userRepository;
        private readonly AppSettings _applicationSettings;
        private readonly DigifitContext _dbContext;

        public AuthController(IOptions<AppSettings> _applicationSettings, DigifitContext dbContext)
        {
            this._applicationSettings = _applicationSettings.Value;
            this._dbContext = dbContext;
            this.userRepository = new UserRepository(dbContext);
        }

        public AuthenticationResponse JWTGenerator(User user)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(this._applicationSettings.Secret);

            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new[] { new Claim("id", user.Id.ToString()) }),
                Expires = DateTime.UtcNow.AddMinutes(1),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha512Signature)
            };

            var token = tokenHandler.CreateToken(tokenDescriptor);
            var EncrypterToken = tokenHandler.WriteToken(token);
            var TokenValidUntil = tokenDescriptor.Expires;

            var RefreshToken = Guid.NewGuid().ToString();
            var RefreshTokenValidUntil = tokenDescriptor.Expires.Value.AddDays(7);

            var existingRefreshToken = _dbContext.RefreshTokens.FirstOrDefault(rt => rt.User.Id == user.Id);

            if (existingRefreshToken != null)
            {
                if (existingRefreshToken.ExpirationDate >= DateTime.UtcNow)
                {
                    // A meglévő refresh token még érvényes, visszaadjuk azt
                    return new AuthenticationResponse
                    {
                        UserId = user.Id.ToString(),
                        Token = EncrypterToken,
                        RefreshToken = existingRefreshToken.Token,
                        TokenValidUntil = TokenValidUntil.Value,
                        RefreshTokenValidUntil = existingRefreshToken.ExpirationDate
                    };
                }
                else
                {
                    var newRefreshTokenValue = Guid.NewGuid().ToString();
                    var newRefreshTokenExpiration = tokenDescriptor.Expires.Value.AddDays(7);
                    _dbContext.RefreshTokens.Remove(existingRefreshToken);

                    var newRefreshToken = new RefreshToken
                    {
                        Token = newRefreshTokenValue,
                        ExpirationDate = newRefreshTokenExpiration,
                        User = user
                    };

                    _dbContext.RefreshTokens.Add(newRefreshToken);
                    _dbContext.SaveChanges();

                    return new AuthenticationResponse
                    {
                        UserId = user.Id.ToString(),
                        Token = EncrypterToken,
                        RefreshToken = newRefreshTokenValue,
                        TokenValidUntil = TokenValidUntil.Value,
                        RefreshTokenValidUntil = newRefreshTokenExpiration
                    };

                }
            }
            else
            {
                // Nincs még refresh token az adatbázisban, hozzáadjuk
                var newRefreshToken = new RefreshToken
                {
                    Token = RefreshToken,
                    ExpirationDate = RefreshTokenValidUntil,
                    User = user
                };

                Console.WriteLine(newRefreshToken);
                _dbContext.RefreshTokens.Add(newRefreshToken);
                _dbContext.SaveChanges();

                return new AuthenticationResponse
                {
                    UserId = user.Id.ToString(),
                    Token = EncrypterToken,
                    RefreshToken = RefreshToken,
                    TokenValidUntil = TokenValidUntil.Value,
                    RefreshTokenValidUntil = RefreshTokenValidUntil
                };
            }
        }


        public static User GetUserFromJwtPayload(GoogleJsonWebSignature.Payload googlePayload)
        {
            var config = new MapperConfiguration(cfg => {
                cfg.CreateMap<GoogleJsonWebSignature.Payload, User>()
                    .ForMember(dest => dest.UserName, opt => opt.MapFrom(src => src.Name))
                    .ForMember(dest => dest.Email, opt => opt.MapFrom(src => src.Email))
                    .ForMember(dest => dest.FirstName, opt => opt.MapFrom(src => src.GivenName))
                    .ForMember(dest => dest.LastName, opt => opt.MapFrom(src => src.FamilyName))
                    .ForMember(dest => dest.GoogleId, opt => opt.MapFrom(src => src.JwtId)); // Módosítás: GoogleId beállítása a JWTid mezőből
            });

            var mapper = config.CreateMapper();

            var user = mapper.Map<GoogleJsonWebSignature.Payload, User>(googlePayload);

            return user;
        }


        [HttpPost("LoginWithGoogle")]
        public async Task<IActionResult> LoginWithGoogle([FromBody] string credential)
        {
            if (string.IsNullOrEmpty(credential))
            {
                return BadRequest("Bad request");
            }

            var settings = new GoogleJsonWebSignature.ValidationSettings()
            {
                Audience = new List<string> { "694033662565-kdiqe2dnsbelttu50d6u0kk5esdkhbgn.apps.googleusercontent.com" }
            };

            var payload = await GoogleJsonWebSignature.ValidateAsync(credential, settings);

            if (payload == null)
            {
                return BadRequest("Payload is null");
            }

            var user = GetUserFromJwtPayload(payload);
        

            var existingUser = await _dbContext.Users.FirstOrDefaultAsync(u => u.GoogleId == user.GoogleId);

            if (existingUser == null)
            {
                Console.WriteLine(user);
                await _dbContext.Users.AddAsync(user);
                await _dbContext.SaveChangesAsync();
            }

            var token = JWTGenerator(existingUser);

            return Ok(token);
        }


        [HttpPost("LoginWithNeptun")]
        public async Task<IActionResult> LoginWithNeptun([FromBody] LoginRequest loginRequest)
        {
            if (loginRequest == null)
            {
                return BadRequest("Érvénytelen JSON adat.");
            }

            try
            {
                string neptun = loginRequest.Neptun;
                string password = loginRequest.Password;
                string url = loginRequest.Url + "/GetTrainings";
                Console.WriteLine("UserLogin: " + neptun);
                Console.WriteLine("Password: ******" );
                Console.WriteLine("Url: " + url);

                SendRequest sendRequest = new()
                {
                    OnlyLogin = true,
                    UserLogin = neptun,
                    Password = password,
                    CurrentPage = 0,
                    LCID = 1038
                };

                string jsonRequest = JsonConvert.SerializeObject(sendRequest);
                Console.WriteLine("JSON kérés:");
                Console.WriteLine(jsonRequest);

               
                var content = new StringContent(jsonRequest, Encoding.UTF8, "application/json");

                using HttpClient client = new();
                HttpResponseMessage response = await client.PostAsync(url, content);
                response.EnsureSuccessStatusCode();

                string jsonResponse = await response.Content.ReadAsStringAsync();

                dynamic responseObject = JsonConvert.DeserializeObject(jsonResponse);

                string errorMessage = responseObject!.ErrorMessage;
                Console.WriteLine("Hibaüzenet: " + errorMessage);

                var existingUserWithNeptun = await _dbContext.Users.FirstOrDefaultAsync(u => u.Neptun == neptun);

                User newUser = new()
                {
                    Neptun = neptun
                };

                if (string.IsNullOrEmpty(errorMessage) && existingUserWithNeptun == null)
                {

                    Console.WriteLine(newUser);
                    await _dbContext.Users.AddAsync(newUser);
                    await _dbContext.SaveChangesAsync();

                    var token = JWTGenerator(newUser); 

                    Console.WriteLine("Sikeres belépés");
                    return Ok(token);

                }
                else if (string.IsNullOrEmpty(errorMessage) && existingUserWithNeptun != null)
                {

                    var token = JWTGenerator(existingUserWithNeptun);

                    Console.WriteLine("Sikeres belépés");
                    return Ok(token);

                }
                else
                {
                    Console.WriteLine("Hibaüzenet: " + errorMessage);
                    return BadRequest(errorMessage);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                return StatusCode(500, "Hiba történt a feldolgozás során: " + ex.Message);
            }
        }


    }
}

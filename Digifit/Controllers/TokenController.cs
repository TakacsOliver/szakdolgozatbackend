﻿using Digifit.Models.Database;
using Google.Apis.Auth.OAuth2.Requests;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Digifit.Controllers
{
    public class TokenController : Controller
    {
        private readonly IConfiguration _configuration;
        private readonly AppSettings _applicationSettings;
        private readonly DigifitContext _dbContext;


        public TokenController(IConfiguration configuration, IOptions<AppSettings> appSettings, DigifitContext dbContext)
        {
            _configuration = configuration;
            _applicationSettings = appSettings.Value;
            _dbContext = dbContext;
        }

        [HttpGet("api/token/checkValidity")]
        public IActionResult CheckTokenValidity()
        {
            // Token érvényességének ellenőrzése
            if (IsTokenValid())
            {
                return Ok(new { valid = true });
            }
            else
            {
                return Unauthorized(new { valid = false });
            }
        }

        [HttpPost("api/token/refreshToken")]
        public async Task<IActionResult> RefreshToken([FromBody] string refreshToken)
        {
            Console.WriteLine($"Received Token: ",refreshToken);
            var refreshTokenEntity = await _dbContext.RefreshTokens
                .FirstOrDefaultAsync(rt => rt.Token == refreshToken);



            if (refreshTokenEntity != null)
            {
                Guid userId = refreshTokenEntity.UserId; // Ideiglenes változó a jobb olvashatóságért

                // Új token generálása
                var newToken = GenerateToken(userId.ToString());
                var tokenValidUntil = DateTime.UtcNow.AddMinutes(10); // Token érvényességének ideje (10 perc)

                Console.WriteLine($"Received Refresh TokenRRRRRRR: {refreshToken}");
                Console.WriteLine($"Generated TokenRRRRR: {newToken}");
                Console.WriteLine($"Token Valid UntilRRRR: {tokenValidUntil}");

                return Ok(new { token = newToken, tokenValidUntil });
            }
            else
            {
                return Unauthorized();
            }
        }

        private bool IsTokenValid()
        {
            // Token érvényességének ellenőrzése
            string token = Request.Headers["Authorization"].ToString().Replace("Bearer ", "");
            var key = Encoding.ASCII.GetBytes(_configuration[this._applicationSettings.Secret]);

            try
            {
                var tokenHandler = new JwtSecurityTokenHandler();
                tokenHandler.ValidateToken(token, new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(key),
                    ValidateIssuer = false,
                    ValidateAudience = false,
                    ClockSkew = TimeSpan.Zero
                }, out var validatedToken);

                return true;
            }
            catch
            {
                return false;
            }
        }

        /*
        private static bool IsRefreshTokenValid(string refreshToken, User user)
        {
            using var dbContext = new DigifitContext();


            var storedRefreshToken = dbContext.RefreshTokens
                .FirstOrDefault(rt => rt.UserId == user.Id);

            if (storedRefreshToken != null && storedRefreshToken.RefreshToken == refreshToken)
            {
                return true;
            }
            else
            {
                return false;
            }
        }*/


        private string GenerateToken(string userId)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_configuration["ApplicationSettings:Secret"]);

            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new[]
                {
                    new Claim("id", userId)
                }),
                Expires = DateTime.UtcNow.AddMinutes(10),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };

            var token = tokenHandler.CreateToken(tokenDescriptor);
            return tokenHandler.WriteToken(token);
        }
    }
}

﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Digifit.Utils;
using Google.Apis.Auth.OAuth2.Requests;
using Microsoft.EntityFrameworkCore;
using Digifit.DAL;
using Digifit.Models.Database;
using Digifit.Models.Query;
using Digifit.Models.Authentication;

namespace Digifit.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProfileController : ControllerBase
    {
        private readonly DigifitContext _context;
        private readonly JwtTokenHandler _jwtTokenHandler;
        private readonly IUserRepository _userRepository;

        public ProfileController(DigifitContext context, JwtTokenHandler jwtTokenHandler, IUserRepository userRepository)
        {
            _context = context;
            _jwtTokenHandler = jwtTokenHandler;
            _userRepository = userRepository;
        }

        [HttpPost("get-profile")]
        public IActionResult GetProfile([FromBody] Models.Authentication.TokenRequest tokenRequest)
        {
            if (tokenRequest == null || string.IsNullOrEmpty(tokenRequest.Token))
            {
                return BadRequest("Invalid token");
            }

            Console.WriteLine("TokenRequest:");
            Console.WriteLine($"Token: {tokenRequest.Token}");

            if (!_jwtTokenHandler.IsTokenValid(tokenRequest.Token))
            {
                return Unauthorized("Invalid token");
            }
            Guid? userId = _jwtTokenHandler.GetUserIdFromToken(tokenRequest.Token);

            if (userId == null)
            {
                return BadRequest("Invalid TokenId");
            }

            Console.WriteLine($"UserId: {userId}");
            // Egyéb kód a felhasználó adatainak lekérésére

            var user = _context.Users.Find(userId.Value);

            if (user == null)
            {
                return NotFound(); // Vagy más hibakód, ha a felhasználó nem található
            }

            var profileData = new
            {
                user.Neptun,
                user.Birthday,
                user.FirstName,
                user.LastName,
                user.PhoneNumber,
                user.Location,
                user.Email

            };

            Console.WriteLine("Kérés érkezett a /api/profile végpontra.");
            Console.WriteLine("Profile adatok:");
            Console.WriteLine($"Neptun: {profileData.Neptun}");
            Console.WriteLine($"FirstName: {profileData.FirstName}");
            Console.WriteLine($"LastName: {profileData.LastName}");
            Console.WriteLine($"Email: {profileData.Email}");
            Console.WriteLine($"PhoneNumber: {profileData.PhoneNumber}");
            Console.WriteLine($"Location: {profileData.Location}");
            Console.WriteLine($"Birthday: {profileData.Birthday}");

            return Ok(profileData);
        }

        private static DateTime? GetNullableDateTime(DateTime? value)
        {
            if (value.HasValue)
            {
                return value.Value.ToUniversalTime();
            }
            return null;
        }

        [HttpPut("update-profile")]
        public IActionResult UpdateProfile(ProfileUpdate profileUpdate)
        {
            Console.WriteLine("Kérés érkezett:" + profileUpdate);
            if (profileUpdate == null || string.IsNullOrEmpty(profileUpdate.Token))
            {
                return BadRequest("Invalid token");
            }

            if (!_jwtTokenHandler.IsTokenValid(profileUpdate.Token))
            {
                return Unauthorized("Invalid token");
            }

            Guid userId = _jwtTokenHandler.GetUserIdFromToken(profileUpdate.Token);

            var user = _userRepository.GetUserByID(userId);

            if (user == null)
            {
                return NotFound(); // Vagy más hibakód, ha a felhasználó nem található
            }

            // Felhasználó frissítése a kapott adatok alapján
            user.FirstName = profileUpdate.FirstName;
            user.LastName = profileUpdate.LastName;
            user.Email = profileUpdate.Email;
            user.PhoneNumber = profileUpdate.PhoneNumber;
            user.Location = profileUpdate.Location;
            user.Birthday = GetNullableDateTime(profileUpdate.Birthday);


            _userRepository.UpdateUser(user);
            _userRepository.Save();

            return Ok(new { message = "Profile updated successfully" });
        }


    }
}

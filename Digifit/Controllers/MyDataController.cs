﻿using Digifit.DAL;
using Digifit.Models.Authentication;
using Digifit.Models.Database;
using Digifit.Models.Query;
using Digifit.Utils;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.HttpResults;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Text.Json;

namespace Digifit.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MyDataController : ControllerBase
    {
        private readonly DigifitContext _context;
        private readonly JwtTokenHandler _jwtTokenHandler;

        public MyDataController(DigifitContext context, JwtTokenHandler jwtTokenHandler)
        {
            _context = context;
            _jwtTokenHandler = jwtTokenHandler;
        }

        [HttpPost("my-data")]
        public IActionResult MyData([FromBody] MyDataRequest request)
        {
            int defaultPageNumber = 1;
            int defaultItemsPerPage = 20;
            int pageNumber = (request.PageNumber.HasValue && request.PageNumber > 0) ? request.PageNumber.Value : defaultPageNumber;
            int itemsPerPage = (request.ItemsPerPage.HasValue && request.ItemsPerPage > 0) ? request.ItemsPerPage.Value : defaultItemsPerPage;
            string token = request.Token;
            string sortBy = request.SortBy;
            string sortDirection = request.SortDirection;
            string filterBy = request.FilterBy;
            string filterValue = request.FilterValue;

            // Most már használhatod az adatokat
            // Például kiírás konzolra:
            System.Console.WriteLine("Token: " + token);
            System.Console.WriteLine("PageNumber: " + pageNumber);
            System.Console.WriteLine("ItemsPerPage: " + itemsPerPage);
            System.Console.WriteLine("SortBy: " + sortBy);
            System.Console.WriteLine("SortDirection: " + sortDirection);
            System.Console.WriteLine("FilterBy: " + filterBy);
            System.Console.WriteLine("FilterValue: " + filterValue);

            if (string.IsNullOrEmpty(token))
            {
                return BadRequest("Invalid token");
            }

            if (!_jwtTokenHandler.IsTokenValid(token))
            {
                return StatusCode(498, "Token expired or invalid");
            }

            Guid userId = _jwtTokenHandler.GetUserIdFromToken(token);

            var query = _context.UserWorkoutExercise
                .Include(uwe => uwe.Workout)
                .Include(uwe => uwe.Exercise)
                .Where(uwe => uwe.User.Id == userId);

            if (!string.IsNullOrEmpty(filterValue) && !string.IsNullOrEmpty(filterBy))
            {
                switch (filterBy)
                {
                    case "Name":
                        query = query.Where(uwe => uwe.Workout.Name.Equals(filterValue));
                        break;
                    case "startDate":
                        query = query.Where(uwe => uwe.Workout.StartDate.Equals(filterValue));
                        break;
                    case "endDate":
                        query = query.Where(uwe => uwe.Workout.EndDate.Equals(filterValue));
                        break;
                }
            }

            if (!string.IsNullOrEmpty(sortBy))
            {
                query = sortBy switch
                {
                    "startDate" => sortDirection == "desc" ? query.OrderByDescending(uwe => uwe.Workout.StartDate) : query.OrderBy(uwe => uwe.Workout.StartDate),
                    "endDate" => sortDirection == "desc" ? query.OrderByDescending(uwe => uwe.Workout.EndDate) : query.OrderBy(uwe => uwe.Workout.EndDate),
                    "Name" => sortDirection == "desc" ? query.OrderByDescending(uwe => uwe.Workout.Name) : query.OrderBy(uwe => uwe.Workout.Name),
                    _ => query.OrderBy(uwe => uwe.Workout.StartDate),
                };
            }
            else
            {
                query = query.OrderBy(uwe => uwe.Workout.StartDate);
            }

            int startIndex = (pageNumber - 1) * itemsPerPage;
            var groupedQuery = query.GroupBy(uwe => uwe.Workout); // Csoportosítás az edzések alapján
            var pagedData = groupedQuery
                .OrderByDescending(group => group.Key.StartDate) // Rendezés a kezdési idő szerint
                .Skip(startIndex)
                .Take(itemsPerPage)
                .Select(group => new
                {
                    Workout = group.Key,
                    AveragePerformance = group.ToList().Average(uwe => uwe.Performance)
                })
                .ToList();

            foreach (var item in pagedData)
            {
                System.Console.WriteLine("Workout Name: " + item.Workout.Name);
                System.Console.WriteLine("Workout startdate: " + item.Workout.StartDate);
                System.Console.WriteLine("Workout enddate: " + item.Workout.EndDate);
                System.Console.WriteLine("Average Performance: " + item.AveragePerformance);

            }

            return Ok(pagedData);

        }

        [HttpPost("Exercise")]
        public IActionResult MyData([FromBody] ExerciseRequest request)
        {
            string token = request.Token;
            Guid workoutId = request.WorkoutId;


            if (string.IsNullOrEmpty(token))
            {
                return BadRequest("Invalid token");
            }

            if (!_jwtTokenHandler.IsTokenValid(token))
            {
                return StatusCode(498, "Token expired or invalid"); // 498: Token expired/invalid (custom code)
            }

            Guid userId = _jwtTokenHandler.GetUserIdFromToken(token);

            var exercises = _context.UserWorkoutExercise
                .Where(uwe => uwe.User.Id == userId && uwe.Workout.WorkoutId == workoutId)
                .Select(uwe => new
                {
                    uwe.Exercise.ExerciseId,
                    uwe.Exercise.Name,
                    uwe.Exercise.Duration,
                    uwe.Performance
                })
                .ToList();


            foreach (var item in exercises)
            {
                System.Console.WriteLine("Id: " + item.ExerciseId);
                System.Console.WriteLine("Name: " + item.Name);
                System.Console.WriteLine("Duration: " + item.Duration);
                System.Console.WriteLine("Performance: " + item.Performance);

            }

            return Ok(exercises);
        }



    }
}



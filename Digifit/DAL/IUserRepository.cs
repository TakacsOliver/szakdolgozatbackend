﻿using System;
using System.Collections.Generic;
//using ContosoUniversity.Models;
using Digifit.Models.Database;

namespace Digifit.DAL
{
    public interface IUserRepository : IDisposable
    {
        IEnumerable<User> GetUsers();
        User GetUserByID(Guid userId);
        User GetUserByNeptun(string Neptun);
        void InsertUser(User user);
        void DeleteUser(Guid userID);
        void UpdateUser(User user);
        void Save();
    }
    
}

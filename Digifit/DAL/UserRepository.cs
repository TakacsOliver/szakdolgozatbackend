﻿using Digifit.Models.Database;
using Microsoft.EntityFrameworkCore;

namespace Digifit.DAL
{
    public class UserRepository : IUserRepository, IDisposable
    {
        private readonly DigifitContext _context;

        public UserRepository(DigifitContext _context)
        {
            this._context = _context;
        }

        
        public IEnumerable<User> GetUsers()
        {
            return _context.Users.ToList();
        }

        public User GetUserByID(Guid userId)
        {
            return _context.Users.Find(userId);
        }
        public User GetUserByNeptun(string Neptun)
        {
            return _context.Users.Find(Neptun);
        }

        public void InsertUser(User user)
        {
           _context.Users.Add(user);
        }

        public void DeleteUser(Guid userID)
        {
            User user = _context.Users.Find(userID);
            _context.Users.Remove(user);
        }
        
        public void UpdateUser(User user)
        {
            _context.Entry(user).State = EntityState.Modified;
        }

        public void Save()
        {
            _context.SaveChanges();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }


    }
}

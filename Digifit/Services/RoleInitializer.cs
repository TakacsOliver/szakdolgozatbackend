﻿using Digifit.Models.Database;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace Digifit
{
    public static class RoleInitializer
    {
        public static void InitializeRoles(IServiceProvider serviceProvider)
        {
            using (var scope = serviceProvider.CreateScope())
            {
                var services = scope.ServiceProvider;
                try
                {
                    var dbContext = services.GetRequiredService<DigifitContext>();

                    // Ellenőrizzük, hogy van-e már szerepkör az adatbázisban
                    if (!dbContext.Roles.Any())
                    {
                        // Ha nincs, létrehozzuk az új szerepköröket
                        var studentRole = new Role
                        {
                            RoleId = Guid.NewGuid(),
                            RoleName = "student"
                        };
                        dbContext.Roles.Add(studentRole);

                        var teacherRole = new Role
                        {
                            RoleId = Guid.NewGuid(),
                            RoleName = "teacher"
                        };
                        dbContext.Roles.Add(teacherRole);

                        dbContext.SaveChanges();
                    }
                }
                catch (Exception ex)
                {
                    // Hibakezelés
                    Console.WriteLine("Hiba történt az adatbázis inicializálása során: " + ex.Message);
                }
            }
        }
    }
}
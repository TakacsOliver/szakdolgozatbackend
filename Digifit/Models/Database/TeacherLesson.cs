﻿namespace Digifit.Models.Database
{
    public class TeacherLesson
    {
        public Guid TeacherLessonId { get; set; }
        public Guid UserId { get; set; }
        public Guid LessonId { get; set; }

        public User User { get; set; }
        public Lesson Lesson { get; set; }
    }
}

﻿namespace Digifit.Models.Database
{
    public class StudentLesson
    { 
        public Guid StudentLessonId { get; set; }
        public Guid UserId { get; set; }
        public Guid LessonId { get; set; }
        public Guid WorkoutId { get; set; }

        public User User { get; set; }
        public Lesson Lesson { get; set; }
        public Workout Workout { get; set; }
    }
}

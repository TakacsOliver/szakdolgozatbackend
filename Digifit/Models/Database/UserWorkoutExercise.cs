﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Digifit.Models.Database
{
    public class UserWorkoutExercise
    {
        public Guid UserWorkoutExerciseId { get; set; }
        public User User { get; set; }
        public Workout Workout { get; set; }
        public Exercise Exercise { get; set; }

        public double Performance { get; set; }
    }

}
﻿namespace Digifit.Models.Database
{
    public class StudentGrade
    {
        public Guid StudentGradeId { get; set; }
        public Guid UserId { get; set; }
        public Guid LessonId { get; set; }
        public int Grade { get; set; }

        public User User { get; set; }
        public Lesson Lesson { get; set; }
    }
}

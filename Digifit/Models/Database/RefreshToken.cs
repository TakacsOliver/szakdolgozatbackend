﻿using System.ComponentModel.DataAnnotations;

namespace Digifit.Models.Database
{
    public class RefreshToken
    {
        public Guid RefreshTokenId { get; set; }
        public string Token { get; set; }
        public DateTime ExpirationDate { get; set; }

        public Guid UserId { get; set; }
        public User User { get; set; }
    }
}

﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace Digifit.Models.Database
{
    public class Exercise
    {
        public Guid ExerciseId { get; set; }
        public string Name { get; set; }
        public double Duration { get; set; }

        public ICollection<UserWorkoutExercise> UserWorkoutExercises { get; set; }
    }
}
﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Configuration;
using System.Reflection.Emit;


namespace Digifit.Models.Database
{
    public class User : IdentityUser<Guid>
    {
        public string? Neptun { get; set; }
        public string? FirstName { get; set; }
        public string? LastName { get; set; }
        public string? GoogleId { get; set; }
        public DateTime? Birthday { get; set; }
        public string? Location { get; set; }

        public ICollection<Lesson> Lessons { get; set; }
        public ICollection<UserWorkoutExercise> UserWorkoutExercises { get; set; }
        public ICollection<StudentLesson> StudentLessons { get; set; }
        public StudentGrade StudentGrade { get; set; }
        public RefreshToken RefreshToken { get; set; }

    }




    public class DigifitContext : DbContext 
    {
        public DigifitContext()
        {
        }

        public DigifitContext(DbContextOptions<DigifitContext> options) : base(options)
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseNpgsql("Server=localhost;Database=postgres;User Id=postgres;Password=postgres;");
        }

        public DbSet<User> Users { get; set; }
        public DbSet<RefreshToken> RefreshTokens { get; set; }
        public DbSet<Workout> Workouts { get; set; }
        public DbSet<Exercise> Exercises { get; set; }
        public DbSet<UserWorkoutExercise> UserWorkoutExercise { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<UserRole> UserRole { get; set; }
        public DbSet<Lesson> Lessons { get; set; }
        public DbSet<StudentLesson> StudentLesson { get; set; }
        public DbSet<TeacherLesson> TeacherLesson { get; set; }
        public DbSet<StudentGrade> StudentGrade { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);


        }
    }

}
﻿namespace Digifit.Models.Database
{
    public class Lesson
    {
        public Guid LessonId { get; set; }
        public string LessonName { get; set; }

        public ICollection<User> Users { get; set; }
        public ICollection<StudentLesson> StudentLessons { get; set; }
    }
}

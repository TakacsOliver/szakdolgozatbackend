﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Digifit.Models.Database
{
    public class Workout
    {
        public Guid WorkoutId { get; set; }
        public string Name { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }

        public ICollection<UserWorkoutExercise> UserWorkoutExercises { get; set; }
        public ICollection<StudentLesson> StudentLessons { get; set; }
    }
}

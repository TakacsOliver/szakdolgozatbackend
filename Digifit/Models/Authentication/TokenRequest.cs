﻿namespace Digifit.Models.Authentication
{
    public class TokenRequest
    {
        public string Token { get; set; }
    }
}

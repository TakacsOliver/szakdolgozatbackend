﻿namespace Digifit.Models.Authentication
{
    public class LoginRequest
    {
        public string Neptun { get; set; }
        public string Password { get; set; }
        public string Url { get; set; }
    }
}

﻿namespace Digifit.Models.Authentication
{
    public class AuthenticationResponse
    {
        public string UserId { get; set; }
        public string Token { get; set; }
        public string RefreshToken { get; set; }
        public DateTime TokenValidUntil { get; set; }
        public DateTime RefreshTokenValidUntil { get; set; }

    }
}

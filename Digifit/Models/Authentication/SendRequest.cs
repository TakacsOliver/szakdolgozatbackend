﻿namespace Digifit.Models.Authentication
{
    public class SendRequest
    {
        public bool OnlyLogin { get; set; }
        public string UserLogin { get; set; }
        public string Password { get; set; }
        public int CurrentPage { get; set; }
        public int LCID { get; set; }
    }
}

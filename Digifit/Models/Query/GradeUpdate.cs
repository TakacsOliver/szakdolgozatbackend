﻿namespace Digifit.Models.Query
{
    public class GradeUpdate
    {
        public Guid UserId { get; set; }
        public Guid LessonId { get; set; }
        public int Grade { get; set; }
    }
}

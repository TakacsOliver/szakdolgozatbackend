﻿namespace Digifit.Models.Query
{
    public class MyDataRequest
    {
        public string Token { get; set; }
        public int? PageNumber { get; set; }
        public int? ItemsPerPage { get; set; }
        public string SortBy { get; set; }
        public string SortDirection { get; set; }
        public string FilterBy { get; set; }
        public string FilterValue { get; set; }
    }

}

﻿namespace Digifit.Models.Query
{
    public class DetailsRequest
    {
        public Guid UserId { get; set; }
        public Guid WorkoutId { get; set; }
    }

}

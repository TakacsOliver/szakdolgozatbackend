﻿using System.ComponentModel.DataAnnotations;

namespace Digifit.Models.Query
{
    public class ProfileUpdate
    {
        public string Token { get; set; }
        public DateTime? Birthday { get; set; }
        public string? FirstName { get; set; }
        public string? LastName { get; set; }
        public string? Email { get; set; }
        public string? Location { get; set; }
        public string? PhoneNumber { get; set; }
    }
}
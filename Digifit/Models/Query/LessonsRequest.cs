﻿namespace Digifit.Models.Query
{
    public class LessonsRequest
    {
        public Guid lessonId { get; set; }
        public Guid studentId { get; set; }
    }
}

﻿namespace Digifit.Models.Query
{
    public class ExerciseRequest
    {
        public string Token { get; set; }
        public Guid WorkoutId { get; set; }
    }
}

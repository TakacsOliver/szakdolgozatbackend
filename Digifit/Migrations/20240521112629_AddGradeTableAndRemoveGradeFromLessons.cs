﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Digifit.Migrations
{
    /// <inheritdoc />
    public partial class AddGradeTableAndRemoveGradeFromLessons : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Grade",
                table: "StudentLesson");

            migrationBuilder.CreateTable(
                name: "StudentGrade",
                columns: table => new
                {
                    StudentGradeId = table.Column<Guid>(type: "uuid", nullable: false),
                    UserId = table.Column<Guid>(type: "uuid", nullable: false),
                    LessonId = table.Column<Guid>(type: "uuid", nullable: false),
                    Grade = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StudentGrade", x => x.StudentGradeId);
                    table.ForeignKey(
                        name: "FK_StudentGrade_Lessons_LessonId",
                        column: x => x.LessonId,
                        principalTable: "Lessons",
                        principalColumn: "LessonId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_StudentGrade_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_StudentGrade_LessonId",
                table: "StudentGrade",
                column: "LessonId");

            migrationBuilder.CreateIndex(
                name: "IX_StudentGrade_UserId",
                table: "StudentGrade",
                column: "UserId",
                unique: true);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "StudentGrade");

            migrationBuilder.AddColumn<int>(
                name: "Grade",
                table: "StudentLesson",
                type: "integer",
                nullable: true);
        }
    }
}

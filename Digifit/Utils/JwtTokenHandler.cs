﻿using System.IdentityModel.Tokens.Jwt;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Text;

namespace Digifit.Utils
{
    public class JwtTokenHandler
    {
        private readonly IConfiguration _configuration;

        public JwtTokenHandler(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public bool IsTokenValid(string token)
        {
            var key = Encoding.ASCII.GetBytes(_configuration["ApplicationSettings:Secret"]);

            try
            {
                var tokenHandler = new JwtSecurityTokenHandler();
                tokenHandler.ValidateToken(token, new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(key),
                    ValidateIssuer = false,
                    ValidateAudience = false,
                    ClockSkew = TimeSpan.Zero
                }, out var validatedToken);

                return true;
            }
            catch
            {
                return false;
            }
        }

        public Guid GetUserIdFromToken(string token)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_configuration["ApplicationSettings:Secret"]);
            var validationParameters = new TokenValidationParameters
            {
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = new SymmetricSecurityKey(key),
                ValidateIssuer = false,
                ValidateAudience = false
            };

            try
            {
                var claimsPrincipal = tokenHandler.ValidateToken(token, validationParameters, out var validatedToken);
                var userId = claimsPrincipal.FindFirst("id")?.Value;

                if (!string.IsNullOrEmpty(userId) && Guid.TryParse(userId, out Guid result))
                {
                    return result;
                }
                else
                {
                    throw new ArgumentException("Invalid UserId");
                }
            }
            catch (Exception ex)
            {
                // Kezelheted a token dekódolás közbeni hibát
                Console.WriteLine($"Token dekódolás hiba: {ex.Message}");
                throw new ArgumentException("Invalid UserId");
            }
        }
    }
}
